import json
import pandas as pd
from prettytable import PrettyTable
import os, subprocess

def depth_json_to_table(json_data):
    dict_data = json.loads(json_data)

    bid_table = PrettyTable()
    bid_table.field_names = ['Bid Price', 'Bid Quantity', 'Ask Price', 'Ask Quantity']

    for i in range(len(dict_data['bids'])):
        bid_table.add_row(dict_data['bids'][i]+dict_data['asks'][i])


    # Важно! Чтобы консоль очищалась каждый тик нужно включить настройку в Pycharm, описанную ниже
    #If you want the terminal to emulate your OSs terminal, you have to edit the 'Run/Debug Configuration' and tick the box 'Emulate terminal in output console'.
    # Then os.system("cls") will clear the screen on Windows and os.system("clear") on unix

    os.system('cls')
    print(bid_table)
