
####### WebSocketApp library

# root, label = 1, 1
# def click(wsa):
#     print("Close ws")
#     wsa.close()
#
# ### Here are receiving data messages and main handling
# def __on_message(wsa, data):
#     print(json.loads(data))
#     label['text'] = json.loads(data)
#
#     ############ Close connection
#     try:  # used try so that if user pressed other than the given key error will not be shown
#         if keyboard.is_pressed('escape'):  # if key 'escape' is pressed
#             print('Close wss')
#             wsa.close()
#     except:
#         pass
#     ###################################
# def __on_close(wsa):
#     print('### Connection closed ###')
#
#
# ### The main public method of this class. Set socket connection and subscribe it with messages sent to on_messages function
# def run():
#     wss = f'wss://stream.binance.com:9443/ws/btcusdt@kline_1m'
#     global wsa
#     wsa = websocket.WebSocketApp(wss, on_message=__on_message, on_close=__on_close)
#     wsa.run_forever()
#
# if __name__ == "__main__":
#     thread1 = threading.Thread(target=run, name='subscr_thread')
#     thread1.start()
#
#     root = Tk()
#     label = Label(text="Hello Money!")  # создаем текстовую метку
#     root.title('My App')  # заголовок
#     root.geometry("300x250")
#
#     btn = Button(text="Click", command=lambda: click(wsa))
#     btn.pack()
#
#     label.pack()  # размещаем метку в окне
#     root.mainloop()  # отображение окна

########################################### Рабочий метод с помощью websocket-app library
### Эта библиотека работает через коллбеки __on_message, __on_close и т.д.
### Но неудобна тем, что нельзя отправлять messages на сервер во время уже запущенного соединения (метод run_forever())
### (ну по крайней мере, я не нашел способа в документации и на stackoverflow). А messages в данном случае - критичная опция.
### Поэтому отправляет в old


# def __on_message(wsa, data):
#     temp = json.loads(data)
#
#     for i in GUI.bid_table.get_children():
#         GUI.bid_table.delete(i)
#
#     for i in GUI.ask_table.get_children():
#         GUI.ask_table.delete(i)
#
#     for row in temp['bids']:
#         GUI.bid_table.insert("", END, values=tuple(row))
#
#     for row in temp['asks']:
#         GUI.ask_table.insert("", 0, values=tuple(row))
#
#     # print(json.loads(data))
#     # print(threading.enumerate())
#     # GUI.label['text'] = json.loads(data)
#
# def __on_close(wsa):
#     print('Close Connection')
#
# # Creates basic websocket connection and handle it using callback functions
# # It runs in isolated thread
# @worker
# def run():
#     streams = ['btsusdt@kline_1m',
#                'ethusdt@kline_1m']
#
#     subscribe_request = {
#                 "method": "SUBSCRIBE",
#                 "params": streams,
#                 "id": 1
#             }
#
#     wss = f'wss://stream.binance.com:9443/ws/btcusdt@depth5@100ms'
#     #wss = 'wss://fstream-auth.binance.com/stream?streams='
#     global wsa
#     wsa = websocket.WebSocketApp(wss, on_message=__on_message, on_close=__on_close)
#     #wsa.send(json.dumps(subscribe_request))
#     wsa.run_forever()