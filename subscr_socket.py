import asyncio
import websockets
import json
import GUI
from tkinter import *
import threading
import websocket
import Markets
from worker import worker
from worker import async_worker


async def subscribe(stock_name):

    async with websockets.connect(Markets.Markets[stock_name].stream_url) as ws:
        await ws.send(json.dumps(Markets.Markets[stock_name].subscribe_request))
        print(await ws.recv())
        market = Markets.Markets[stock_name]()
        #market = Markets.Bybit()
        a = {}

        async for message in ws:
            data = json.loads(message)
            #print(data)

            a = market.fill_tables(data, a)

            if GUI.close_thread: return




