import threading
import json
import PySimpleGUI as sg
import subscr_socket
import asyncio
import websockets
from tkinter import *
import Markets
import _thread
from worker import worker
from worker import async_worker
from tkinter import ttk


def click_subscr_btn():

    if len(market_selector.get()) == 0: return # return if nothing has changed in gui selector

    run_subscr_btn.config(text='Connected')
    run_subscr_btn.config(state='disabled')

    close_subscr_btn.config(text='Disconnect')
    close_subscr_btn.config(state='normal')

    global close_thread
    close_thread = False

    # Im running new thread for async websocket subscription.
    # Именно так - через target=asyncio.run и передачей целевого метода через args=(subscr_socket.subscribe(), )
    s_thread = threading.Thread(target=asyncio.run,
                                args=(subscr_socket.subscribe(market_selector.get()), ), # send selector value as argument to subscribe socket function
                                name='subscribe_thread')
    s_thread.start()

def click_close_btn():
    #subscr_socket.wsa.close()
    global close_thread
    close_thread = True

    run_subscr_btn.config(text='Connect')
    run_subscr_btn.config(state='normal')

    close_subscr_btn.config(state='disabled')

    print(threading.enumerate())

def create_window():
    # Elements
    global root
    global run_subscr_btn
    global close_subscr_btn
    global market_selector

    # Иерархия Windows окна такая: сначала создается root, потом виджет notebook и фреймы, которые к нему привязаны.
    # Фреймы служат вкладками. Вызываем им всем pack() сразу после объявления
    # Далее вызывается еще один метод add() для всех фреймов, который устанавливает их в качестве табов в Notebook
    # После этого уже можно размещать остальные элементы для каждого фрейма, но передавать им переменную фрейма
    # в качестве родителя при объявлении

    root = Tk()

    ################################## Create Tabs for window
    # создаем набор вкладок
    notebook = ttk.Notebook()
    notebook.pack(expand=True, fill=BOTH)

    # создаем пару фреймвов
    frame1 = ttk.Frame(notebook)
    frame2 = ttk.Frame(notebook)

    frame1.pack(fill=BOTH, expand=True)
    frame2.pack(fill=BOTH, expand=True)

    # добавляем фреймы в качестве вкладок
    notebook.add(frame1, text="Glass")
    notebook.add(frame2, text="Graph")
    ##################################

    market_label = Label(frame1, text='Choose Market')
    market_label.pack(anchor='w', pady=10, padx=20)

    market_selector = ttk.Combobox(frame1, values=list(Markets.Markets.keys()), state='readonly')
    market_selector.pack(pady=10)


    # GLASS TABLE
    columns = ('Price', 'Value')
    global bid_table
    global ask_table

    bid_label = Label(frame1, text="BIDS", foreground='green', font=("Arial", 14))
    bid_label.pack()  # размещаем метку в окне

    bid_table = ttk.Treeview(frame1, columns=columns, show="headings")
    bid_table.pack()

    bid_table.heading("Price", text="Price")
    bid_table.heading("Value", text="Value")

    ask_label = Label(frame1, text="ASKS", foreground='red', font=("Arial", 14))
    ask_label.pack()  # размещаем метку в окне

    ask_table = ttk.Treeview(frame1, columns=columns, show="headings")
    ask_table.pack()

    ask_table.heading("Price", text="Price")
    ask_table.heading("Value", text="Value")


    ### GUI elements for first tab (frame1 - Market Glass)
    run_subscr_btn = Button(frame1, text='Connect', command=click_subscr_btn)
    close_subscr_btn = Button(frame1, text='Disconnect', command=click_close_btn, state='disabled')

    # Layers
    root.title('Money Button v0.1')
    root.geometry("500x800")


    # btn = Button(text="Click", command=lambda: click(wsa))

    # Packaging

    run_subscr_btn.pack(pady=10)
    close_subscr_btn.pack(pady=10)

    ### End first tab Elements



    ### GUI elements for second tab (frame1 - Market Glass)
    ### End second tab Elements


    root.mainloop()  # отображение окна

