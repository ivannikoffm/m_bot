import time
import GUI
from tkinter import *

def clear_tables():
    for i in GUI.bid_table.get_children():
        GUI.bid_table.delete(i)

    for i in GUI.ask_table.get_children():
        GUI.ask_table.delete(i)

class Binance:
    stream_url = 'wss://stream.binance.com:9443/stream?streams='

    streams = [
        'btcusdt@depth5@100ms'
    ]

    subscribe_request = {
        "method": "SUBSCRIBE",
        "params": streams,
        "id": 1
    }
    def fill_tables(self, json_data, a):
        clear_tables()
        for row in json_data['data']['bids']:
            GUI.bid_table.insert("", END, values=tuple(row))

        for row in json_data['data']['asks']:
            GUI.ask_table.insert("", 0, values=tuple(row))

class Bybit:
    stream_url = 'wss://stream.bybit.com/v5/public/spot'

    streams = 'orderbook.50.BTCUSDT'

    subscribe_request = {
        "req_id": "test",
        "op": 'subscribe',
        "args": [
            streams
        ]
    }



    def fill_tables(self, json_data, a):
        bids = a

        if json_data['type'] == 'snapshot':
            bids = {float(price): float(vol) for price, vol in json_data['data']['b']}

        if json_data['type'] == 'delta' and len(bids) > 0:
            new_bids = json_data['data']['b']

            for bid in new_bids:
                if bid[1] == '0':
                    del bids[float(bid[0])]
                else:
                    bids.update({float(bid[0]): float(bid[1])})

        bids = {key: bids[key] for key in sorted(bids.keys(), reverse=True)}
        print(bids)
        clear_tables()

        for row in range(5):
            GUI.bid_table.insert("", END, values=list(bids.items())[row])

        return bids


Markets = {'Binance': Binance, 'Bybit': Bybit}